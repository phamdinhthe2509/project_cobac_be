package com.example.project_cobac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.Resource;

@SpringBootApplication
@EntityScan(basePackages = "com.example.project_cobac.entity")
@EnableScheduling
public class ProjectCobacApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectCobacApplication.class, args);
	}

//	@Resource
//	private Environment environment;
//
//	@Scheduled(cron = "10 * * * * *")
//	public void getProxyInfo() {
//		String proxyHost = environment.getProperty("http.proxyHost");
//		String proxyPort = environment.getProperty("http.proxyPort");
//
//		if (proxyHost != null && proxyPort != null) {
//			System.out.println("Proxy Host: " + proxyHost);
//			System.out.println("Proxy Port: " + proxyPort);
//		} else {
//			System.out.println("Proxy is not configured.");
//		}
//	}
}
