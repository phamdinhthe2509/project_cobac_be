package com.example.project_cobac.service.impl;

import com.example.project_cobac.Utils.SystemCodeDesc;
import com.example.project_cobac.dto.request.AddPlayerRequest;
import com.example.project_cobac.dto.request.PlayerToOwnerRequest;
import com.example.project_cobac.dto.request.UpdateListMoneyGameRequest;
import com.example.project_cobac.dto.request.UpdateMoneyGameRequest;
import com.example.project_cobac.dto.response.BaseResponse;
import com.example.project_cobac.dto.response.GetAllGameTableResponse;
import com.example.project_cobac.dto.response.repo.GetAllPlayerInGameTable;
import com.example.project_cobac.entity.GameTable;
import com.example.project_cobac.entity.Player;
import com.example.project_cobac.repository.GameTableRepo;
import com.example.project_cobac.repository.PlayerRepo;
import com.example.project_cobac.service.PlayerService;
import com.example.project_cobac.service.base.BaseService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class PlayerServiceImpl extends BaseService implements PlayerService {
    @Resource
    private PlayerRepo playerRepo;
    @Resource
    private GameTableRepo gameTableRepo;

    @Resource
    private ModelMapper modelMapper;

    @Override
    @Transactional
    public BaseResponse<?> addPlayer(AddPlayerRequest request) {
        try {
            String[] listName = request.getNames().split("[;,]");
            List<Player> players = Stream.of(listName)
                    .map(e -> new Player(e.toUpperCase(), true, 0L))
                    .collect(Collectors.toList());
            playerRepo.saveAll(players);
            return response(new ArrayList<>(), SystemCodeDesc.SUCCESS_CODE, SystemCodeDesc.SUCCESS_DESC);
        } catch (Exception e) {
            return response(null, SystemCodeDesc.ERROR_CODE, SystemCodeDesc.ERROR_DESC);
        }
    }


    @Override
    @Transactional
    public BaseResponse<?> playerToOwn(PlayerToOwnerRequest request) {
        try {
            playerRepo.updateStatusOwn(request.getId());
            List<Long> gameTableEntity = gameTableRepo.findIdMaid(request.getId(), request.getIdN());

            List<Long> listIdN = request.getIdN().stream()
                    .filter(e -> !gameTableEntity.contains(e))
                    .collect(Collectors.toList());
            if (!listIdN.isEmpty()) {
                List<GameTable> listGameTableEntity = listIdN
                        .parallelStream()
                        .map(e ->
                                new GameTable(request.getId(), e, 0L)
                        ).collect(Collectors.toList());
                gameTableRepo.saveAll(listGameTableEntity);
                return response(new ArrayList<>(), SystemCodeDesc.SUCCESS_CODE, SystemCodeDesc.SUCCESS_DESC);
            }
            return response(null, SystemCodeDesc.SUCCESS_CODE, SystemCodeDesc.SUCCESS_DESC);
        } catch (Exception e) {
            return response(null, SystemCodeDesc.ERROR_CODE, SystemCodeDesc.ERROR_DESC);
        }
    }

    @Override
    public BaseResponse<?> getAllPlayer() {
        try {
            return response(playerRepo.findAllByOrderByIdAsc(), SystemCodeDesc.SUCCESS_CODE, SystemCodeDesc.SUCCESS_DESC);
        } catch (Exception e) {
            return response(null, SystemCodeDesc.ERROR_CODE, SystemCodeDesc.ERROR_DESC);
        }
    }

    @Override
    public BaseResponse<?> getAllGameTable(PlayerToOwnerRequest request) {
        try {
            List<GetAllPlayerInGameTable> res = gameTableRepo.getAll(request.getId());
            List<GetAllGameTableResponse> responses = res.parallelStream()
                    .map(e -> modelMapper.map(e, GetAllGameTableResponse.class))
                    .collect(Collectors.toList());
            return response(responses, SystemCodeDesc.SUCCESS_CODE, SystemCodeDesc.SUCCESS_DESC);
        } catch (Exception e) {
            return response(null, SystemCodeDesc.ERROR_CODE, SystemCodeDesc.ERROR_DESC);
        }
    }

    @Override
    public BaseResponse<?> updateMoneyGameTable(UpdateListMoneyGameRequest request) {
        try {
            List<UpdateMoneyGameRequest> requestList = request.getList();
            List<Player> players = ((List<Player>) playerRepo.findAll());
            for (Player player : players) {
                if (player.getId().equals(request.getIdOwn())) {
                    player.setMoney((-request.getSum()) + player.getMoney());
                    continue;
                }
                for (UpdateMoneyGameRequest request1 : requestList) {
                    if (request1.getIdMaid().equals(player.getId())) {
                        player.setMoney(request1.getMoneyNowPlay() + player.getMoney());
                        break;
                    }
                }
            }
            List<GameTable> gameTableList =
                    requestList.parallelStream()
                            .map(e -> new GameTable(e.getId(), e.getIdOwn(), e.getIdMaid(), e.getMoneySave()))
                            .collect(Collectors.toList());
            gameTableRepo.saveAll(gameTableList);
            playerRepo.saveAll(players);
            return response(new ArrayList<>(), SystemCodeDesc.ERROR_CODE, SystemCodeDesc.ERROR_DESC);
        } catch (Exception e) {
            return response(null, SystemCodeDesc.ERROR_CODE, SystemCodeDesc.ERROR_DESC);
        }
    }
}
