package com.example.project_cobac.service.base;

import com.example.project_cobac.dto.response.BaseResponse;

public abstract class BaseService<T> {

    protected BaseResponse<T> response (T data, String code, String desc) {
        BaseResponse<T> baseResponse = new BaseResponse<T>();
        baseResponse.setData(data);
        baseResponse.setCode(code);
        baseResponse.setDesc(desc);
        return baseResponse;
    }
}
