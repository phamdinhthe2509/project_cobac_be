package com.example.project_cobac.service;

import com.example.project_cobac.dto.request.AddPlayerRequest;
import com.example.project_cobac.dto.request.PlayerToOwnerRequest;
import com.example.project_cobac.dto.request.UpdateListMoneyGameRequest;
import com.example.project_cobac.dto.response.BaseResponse;
import org.springframework.stereotype.Component;

@Component
public interface PlayerService {

    BaseResponse<?> addPlayer(AddPlayerRequest request);
    BaseResponse<?> playerToOwn(PlayerToOwnerRequest request);
    BaseResponse<?> getAllPlayer();
    BaseResponse<?> getAllGameTable(PlayerToOwnerRequest request);
    BaseResponse<?> updateMoneyGameTable(UpdateListMoneyGameRequest request);
}
