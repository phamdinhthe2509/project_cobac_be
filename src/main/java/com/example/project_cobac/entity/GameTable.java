package com.example.project_cobac.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "game_table")
public class GameTable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "idOwn")
    private Long idOwn;
    @Column(name = "idMaid")
    private Long idMaid;
    @Column(name = "moneyPlay")
    private Long moneyPlay;

    public GameTable(Long idOwn, Long idMaid, Long moneyPlay) {
        this.idOwn = idOwn;
        this.idMaid = idMaid;
        this.moneyPlay = moneyPlay;
    }
}
