package com.example.project_cobac.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity(name = "player")
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "status")
    private boolean status;
    @Column(name = "status_own")
    private boolean statusOwn;
    @Column(name = "money")
    private Long money;

    public Player() {
    }

    public Player(String name, boolean status, Long money) {
        this.name = name;
        this.status = status;
        this.money = money;
    }
}
