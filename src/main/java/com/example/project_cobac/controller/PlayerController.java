package com.example.project_cobac.controller;

import com.example.project_cobac.dto.request.AddPlayerRequest;
import com.example.project_cobac.dto.request.PlayerToOwnerRequest;
import com.example.project_cobac.dto.request.UpdateListMoneyGameRequest;
import com.example.project_cobac.dto.response.BaseResponse;
import com.example.project_cobac.service.PlayerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@CrossOrigin("*")
public class PlayerController {

    @Resource
    private PlayerService playerService;

    @PostMapping("/add-player")
    public ResponseEntity<?> addPlayer(@RequestBody AddPlayerRequest request) {
        BaseResponse<?> response = playerService.addPlayer(request);
        return ResponseEntity.ok().body(response);
    }
    @PostMapping("/player-to-own")
    public ResponseEntity<?> playerToOwn(@RequestBody PlayerToOwnerRequest request) {
        BaseResponse<?> response = playerService.playerToOwn(request);
        return ResponseEntity.ok().body(response);
    }
        @PostMapping("/get-all-player")
    public ResponseEntity<?> getAllPlayer() {
        BaseResponse<?> response = playerService.getAllPlayer();
        return ResponseEntity.ok().body(response);
    }
    @PostMapping("/get-all-game-table")
    public ResponseEntity<?> getAllGameTable(@RequestBody PlayerToOwnerRequest request) {
        BaseResponse<?> response = playerService.getAllGameTable(request);
        return ResponseEntity.ok().body(response);
    }
    @PostMapping("/update-money-game-table")
    public ResponseEntity<?> getAllGameTable(@RequestBody UpdateListMoneyGameRequest request) {
        BaseResponse<?> response = playerService.updateMoneyGameTable(request);
        return ResponseEntity.ok().body(response);
    }
}
