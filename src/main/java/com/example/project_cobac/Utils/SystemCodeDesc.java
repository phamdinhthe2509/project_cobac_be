package com.example.project_cobac.Utils;

public interface SystemCodeDesc {
    String SUCCESS_CODE = "OK";
    String SUCCESS_DESC = "Thành công";
    String ERROR_CODE = "ERROR";
    String ERROR_DESC = "Thất bại";
}
