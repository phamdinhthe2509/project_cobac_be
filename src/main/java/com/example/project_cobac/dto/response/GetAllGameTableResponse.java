package com.example.project_cobac.dto.response;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GetAllGameTableResponse {
    private Long id;
    private String name;
    private Long idMaid;
    private Long idOwn;
    private boolean statusPlay;
    private Long moneyPlay;
}
