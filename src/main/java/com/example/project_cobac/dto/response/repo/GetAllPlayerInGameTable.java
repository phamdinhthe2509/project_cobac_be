package com.example.project_cobac.dto.response.repo;

public interface GetAllPlayerInGameTable {

    Long getId();

    String getName();
    Long getIdMaid();
    Long getIdOwn();
    Long getMoneyPlay();
    boolean getStatusPlay();
}
