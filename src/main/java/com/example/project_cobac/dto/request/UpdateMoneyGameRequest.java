package com.example.project_cobac.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateMoneyGameRequest {
    private Long id;
    private Long idOwn;
    private Long idMaid;
    private Long moneySave;
    private Long moneyNowPlay;
}
