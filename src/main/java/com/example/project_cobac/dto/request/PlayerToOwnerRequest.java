package com.example.project_cobac.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlayerToOwnerRequest {

    private Long id;
    private List<Long> idN;
}
