package com.example.project_cobac.repository;

import com.example.project_cobac.entity.Player;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository("PlayerRepo")
public interface PlayerRepo extends CrudRepository<Player, Long> {

    List<Player> findAllByIdNot(Long id);

    List<Player> findAllByOrderByIdAsc();

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "update player SET status_own = " +
            " CASE " +
            "   WHEN id = :id THEN true " +
            " ELSE false " +
            " END where id = :id or id != :id")
    void updateStatusOwn(@Param("id") Long id);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "update player set money = money + :moneyIn where id = :id")
    void updateMoney(@Param("moneyIn") Long money, @Param("id") Long id);
}
