package com.example.project_cobac.repository;

import com.example.project_cobac.dto.response.GetAllGameTableResponse;
import com.example.project_cobac.dto.response.repo.GetAllPlayerInGameTable;
import com.example.project_cobac.entity.GameTable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository("GameTableRepo")
public interface GameTableRepo extends CrudRepository<GameTable, Long> {

    @Query(nativeQuery = true, value = "select id_maid as id from game_table where id_own = :id and id_maid in (:idN)")
    List<Long> findIdMaid (Long id, List<Long> idN);


    @Query(nativeQuery = true, value = "select t_p.name as name, t_p.status as statusPlay, t_gp.id as id, t_gp.id_own as idOwn, t_gp.id_maid as idMaid, t_gp.money_play as moneyPlay" +
            " from player t_p inner join game_table t_gp" +
            " on t_p.id = t_gp.id_maid " +
            " where t_gp.id_own = :id " +
            " order by t_gp.id_maid asc")
    List<GetAllPlayerInGameTable> getAll(Long id);

}
